import sys,os
try:
    sys.path.append(os.getcwd())
except:
    print ('could not import current working directory, if running in IDE ignore')

from experimentation import *
import benchmark

INDIVIDUALS = 400
MAX_GENERATIONS = 3000
DIMENSIONS = 10
trials = 20


def evaluate(benchmark_name, fitness):

    standard_reports = list()
    dgea_reports = list()
    island_reports = list()
    artificial_reports = list()
    dichotomy_reports = list()

    # Rastrigin fitness function to use for evaluation

    #######################################################
    # Script all experiments

    # Standard individual generation
    standard = Experiment("Short 20D", benchmark_name, "Standard", INDIVIDUALS, MAX_GENERATIONS, DIMENSIONS, fitness.threshold)
    standard.repl_rate = 0.7  # Favor crossover not as strongly

    # need a little bit of exploration in artificial approach
    standard.blx_alpha = 0.2

    standard.mut_rate = 0.05

    # Minimum distance between artificially generated
    # individuals and any individual in current population
    standard.special['artificial_threshold'] = 0

    # Every time pollination occurs replace 5% of individuals
    standard.special['artificial_ratio'] = 0

    standard.special['diversity_threshold'] = 0

    # DGEA
    dgea = Experiment("Short 20D", benchmark_name, "DGEA", INDIVIDUALS, MAX_GENERATIONS, DIMENSIONS, fitness.threshold)
    dgea.repl_rate = 0.9  # Favor crossover

    # dgea is supposed to introduce exploration using
    # mutation so a high alpha is pointless (ability of crossover to introduce new material)
    dgea.blx_alpha = 0.2

    # Applied only in exploration mode
    dgea.mut_rate = 0.75

    # Average distance between individuals
    dgea.special["expl_threshold"] = 0.025

    # Artificial individual generation
    artificial = Experiment("Short 20D", benchmark_name, "Artificial", INDIVIDUALS, MAX_GENERATIONS, DIMENSIONS, fitness.threshold)
    artificial.repl_rate = 0.7  # Favor crossover not as strongly

    # need a little bit of exploration in artificial approach
    artificial.blx_alpha = 0.2

    artificial.mut_rate = 0.05

    # Minimum distance between artificially generated
    # individuals and any individual in current population
    artificial.special['artificial_threshold'] = 0.2

    # Every time pollination occurs replace 5% of individuals
    artificial.special['artificial_ratio'] = 0.05

    artificial.special['diversity_threshold'] = 0.05

    # Dichotomy
    # 400 individuals, 10000 gens, 20 dimensions, distance of best solution from global min
    dichotomy = Experiment("Dichotomy 20D", benchmark_name, "Dichotomy", INDIVIDUALS, MAX_GENERATIONS, DIMENSIONS, fitness.threshold)
    dichotomy.repl_rate = 0.7        # Favor crossover

    # favor crossover as introducing new material
    dichotomy.blx_alpha = 0.2

    dichotomy.mut_rate = 0.05

    # Average distance between individuals
    dichotomy.special["focus"] = 0.01
    dichotomy.special["hof_size"] = 10

    # Islands experiment definition
    islands = Experiment("Islands 20D", benchmark_name, "Islands", INDIVIDUALS, MAX_GENERATIONS, DIMENSIONS, fitness.threshold)
    islands.repl_rate = 0.7        # Favor crossover not as strongly

    # favor crossover as source of new material
    islands.blx_alpha = 0.2

    # Applied only in exploration mode
    islands.mut_rate = 0.05

    # Nunmber of islands
    islands.special["islands"] = 4

    # Max generations between pollination
    islands.special['pollination_interval'] = islands.max_generations / 20

    # Every time pollination occurs replace 5% of individuals
    islands.special['pollination_ratio'] = 0.05

    # Diversity threshold below which islands are used
    islands.special['diversity_threshold'] = 0.05

    #####################################################
    # Conduct all experiments

    for _ in range(0, trials):

        print str(dichotomy)
        dichotomy_results = dichotomy_approach(dichotomy, fitness)
        print "Report for Dichotomy strategy used on %s" % benchmark_name
        print str(dichotomy_results)

        print str(islands)
        island_results = island_approach(islands, fitness)
        print "Report for Island strategy used on %s" % benchmark_name
        print str(island_results)

        print str(artificial)
        artificial_results = artificial_approach(artificial, fitness)
        print "Report for Artificial strategy used on %s" % benchmark_name
        print str(artificial_results)

        print str(dgea)
        dgea_results = dgea_approach(dgea, fitness)
        print "Report for DGEA strategy used on %s" % benchmark_name
        print str(dgea_results)

        print str(standard)
        standard_results = artificial_approach(standard, fitness)
        print "Report for Standard strategy used on %s" % benchmark_name
        print str(standard_results)

        dichotomy_reports.append(dichotomy_results)
        island_reports.append(island_results)
        artificial_reports.append(artificial_results)
        dgea_reports.append(dgea_results)
        standard_reports.append(standard_results)

    ##################################################################################
    # Summarize and graphically display / save collected data

    summarize(dichotomy, dichotomy_reports, '../results/dichotomy')
    dichotomy_eval = evaluate_experiment(dichotomy, dichotomy_reports, '../results/dichotomy', True)

    summarize(islands, island_reports, '../results/islands')
    islands_eval = evaluate_experiment(islands, island_reports, '../results/islands', True)

    summarize(artificial, artificial_reports, '../results/artificial')
    artificial_eval = evaluate_experiment(artificial, artificial_reports, '../results/artificial', True)

    summarize(dgea, dgea_reports, '../results/dgea')
    dgea_eval = evaluate_experiment(dgea, dgea_reports, '../results/dgea', True)

    summarize(standard, standard_reports, '../results/standard')
    standard_eval = evaluate_experiment(standard, standard_reports, '../results/standard', True)

    print 'Evaluation run saved'

    ##########################################################################
    # Create graph comparing fitness of every approach

    data = [standard_eval['fitness_record'], dgea_eval['fitness_record'], islands_eval['fitness_record'],
            artificial_eval['fitness_record'], dichotomy_eval['fitness_record']]
    build_multi_graph("Fitness vs. Generations (%s)" % benchmark_name,
                      "Generations", "fitness", data, '../results/%s.png' % benchmark_name)

    print 'Graphed fitness over time'


fitness_function = benchmark.create_ackley(DIMENSIONS, 0.01)
evaluate("ackley", fitness_function)

fitness_function = benchmark.create_rastrigin(DIMENSIONS, 0.01)
evaluate("rastrigin", fitness_function)
#
fitness_function = benchmark.create_schwefel(DIMENSIONS, 0.1)
evaluate("schwefel", fitness_function)

fitness_function = benchmark.create_rosenbrock(DIMENSIONS, 0.1)
evaluate("rosenbrock", fitness_function)

# fitness_function = benchmark.create_schaffer(DIMENSIONS, 0.01)
# evaluate("schaffer", fitness_function)
