import sys,os
try:
    sys.path.append(os.getcwd())
except:
    print ('could not import current working directory, if running in IDE ignore')

from experimentation import *
import benchmark

# 100 individuals, 1000 gens, 5 dimensions, distance of best solution from global min
exp1 = Experiment("Short 20D", "rastrigin", "Island", 400, 10000, 20, 0.01)
exp1.repl_rate = 0.7        # Favor crossover not as strongly

# favor crossover as source of new material
exp1.blx_alpha = 0.3

# Applied only in exploration mode
exp1.mut_rate = 0.05

# Average distance between individuals
exp1.special["islands"] = 4
exp1.special['pollination_interval'] = exp1.max_generations / 20

# Every time pollination occurs replace 5% of individuals
exp1.special['pollination_ratio'] = 0.05

exp1.special['diversity_threshold'] = 0.008

print 'Attempting experiment\n'
print exp1

fitness = benchmark.create_rastrigin(exp1.num_genes, exp1.threshold)
results = island_approach(exp1, fitness)

print str(results)
