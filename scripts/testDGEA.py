import sys,os
try:
    sys.path.append(os.getcwd())
except:
    print ('could not import current working directory, if running in IDE ignore')

from experimentation import *
import benchmark

# 100 individuals, 1000 gens, 5 dimensions, distance of best solution from global min
exp1 = Experiment("Short 20D", "rastrigin", "DGEA", 400, 10000, 20, 0.01)
exp1.repl_rate = 0.9        # Favor crossover

# dgea is supposed to introduce exploration using
# mutation so a high alpha is pointless (ability of crossover to introduce new material)
exp1.blx_alpha = 0.3

# Applied only in exploration mode
exp1.mut_rate = 0.75

# Average distance between individuals
exp1.special["expl_threshold"] = 0.004

print 'Attempting experiment\n'
print exp1

fitness = benchmark.create_rastrigin(exp1.num_genes, exp1.threshold)
results = dgea_approach(exp1, fitness)

print str(results)
