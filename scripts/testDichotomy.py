import sys,os
try:
    sys.path.append(os.getcwd())
except:
    print ('could not import current working directory, if running in IDE ignore')

from experimentation import *
import benchmark

# 100 individuals, 1000 gens, 5 dimensions, distance of best solution from global min
exp1 = Experiment("Short 20D", "rastrigin", "Dichotomy", 400, 10000, 20, 0.01)
exp1.repl_rate = 0.7        # Favor crossover

# favor crossover as introducing new material
exp1.blx_alpha = 0.3

# Applied only in exploration mode
exp1.mut_rate = 0.05

# Average distance between individuals
exp1.special["focus"] = 0.01
exp1.special["hof_size"] = 10

exp1.recording_interval = 10

print 'Attempting experiment\n'
print exp1

reports = list()
for _ in range(0, 5):

    fitness = benchmark.create_rastrigin(exp1.num_genes, exp1.threshold)
    results = dichotomy_approach(exp1, fitness)

    print str(results)
    reports.append(results)

summarize(exp1, reports, '../results/dichotomy')
evaluate_experiment(exp1, reports, '../results/dichotomy')
