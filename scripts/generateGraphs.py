import sys,os
try:
    sys.path.append(os.getcwd())
except:
    print ('could not import current working directory, if running in IDE ignore')

from graphs.graph import *

print 'Generating Sphere'
test_sphere('../graphs/viz/sphere.png')

print 'Generating Rosenbrock'
test_rosenbrock('../graphs/viz/rosenbrock.png')

print 'Generating Ackley'
test_ackley('../graphs/viz/ackley.png')

print 'Generating Rastrigin'
test_rastrigin('../graphs/viz/rastrigin.png')

print 'Generating Schwefel'
test_schwefel('../graphs/viz/schwefel.png')

print 'Generating Schaffer'
test_schaffer('../graphs/viz/schaffer.png')

