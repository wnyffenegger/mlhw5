import sys,os
try:
    sys.path.append(os.getcwd())
except:
    print ('could not import current working directory, if running in IDE ignore')

from experimentation import *
import benchmark

# 100 individuals, 1000 gens, 5 dimensions, distance of best solution from global min
exp1 = Experiment("Short 20D", "rastrigin", "Artificial", 400, 10000, 20, 0.01)
exp1.repl_rate = 0.7        # Favor crossover not as strongly

# need a little bit of exploration in artificial approach
exp1.blx_alpha = 0.2

# Applied only in exploration mode
exp1.mut_rate = 0.05

# Minimum egal distance between artificially generated
# individuals and any individual in current population
exp1.special['artificial_threshold'] = 0.2

# Every time pollination occurs replace 5% of individuals
exp1.special['artificial_ratio'] = 0.05

exp1.special['diversity_threshold'] = 0.02

print 'Attempting experiment\n'
print exp1

fitness = benchmark.create_rastrigin(exp1.num_genes, exp1.threshold)
results = artificial_approach(exp1, fitness)

print str(results)
