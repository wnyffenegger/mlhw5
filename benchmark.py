import math

# Tests for benchmark functions are in graph.py
# Tests consist of drawing functions and observing whether those surface plots match
# the expected shape


def distance(vector1, vector2):
    """
    Calculate Euclidean distance between two vectors
    :param vector1:
    :type vector1: list(float)
    :param vector2:
    :type vector2: list(float)
    :rtype: float
    """

    least_square = 0
    for i in range(0, len(vector1)):
        least_square += pow(vector1[i] - vector2[i], 2)

    return math.sqrt(least_square)


class Bounds(dict):

    def __init__(self, *arg, **kw):
        # Somehow this gives us the fields by calling dict constructor
        super(Bounds, self).__init__(*arg, **kw)

    def add_bound(self, gene_num, minimum, maximum):
        """
        Add a bound for a dimension given by the number of the gene
        :param gene_num: number of gene (index)
        :type gene_num: int
        :param minimum: minimum number for gene
        :type minimum: float
        :param maximum: maximum number for gene
        :type maximum: float
        :return:
        """
        self[gene_num] = (minimum, maximum)


class Fitness:

    def __init__(self, bounds, global_optimum, threshold, function):
        """
        Initialize wrapper around fitness
        :param bounds: set of bounds for function
        :type bounds: Bounds
        :param global_optimum: location of minimum to error function
        :type global_optimum: list(float)
        :param threshold: distance between threshold and minimum that triggers satisfaction of search
        :type threshold: float
        :param function:
        :type function:
        :return:
        """
        self.bounds = bounds
        self.global_optimum = global_optimum
        self.threshold = threshold
        self.function = function
        self.diagonal = math.sqrt(sum((maximum - minimum) ** 2 for minimum, maximum in bounds.values()))

    def calculate(self, parameters):
        """
        Given a list of parameters calculate value of fitness function
        :param parameters: set of parameters as vector
        :type parameters: list(float)
        :return: calculated fitness value
        :rtype: float
        """

        return self.function(parameters)

    def reached(self, parameters):
        """
        Given a list of parameters to the function determine proximity to optimum
        :param parameters: vector of parameters to fitness function
        :type parameters: list(float)
        :return: true if reached optimum
        :rtype: bool
        """
        return distance(parameters, self.global_optimum) / len(self.bounds.keys()) <= self.threshold


def test(parameters):
    return sum(parameters)


def build_bounds(dimensions, minimum, maximum):
    """
    Build bounds object for function assuming space is hypercube
    :param dimensions: number of dimensions to function
    :type dimensions: int
    :param minimum: minimum value in each dimension
    :type minimum: float
    :param maximum: maximum value in each dimension
    :type maximum: float
    :return:
    """
    bounds = Bounds()
    for d in range(0, dimensions):
        bounds.add_bound(d, minimum, maximum)

    return bounds


def sphere(parameters):
    """
    Implementation of the n dimensional shifted sphere function for benchmarking genetic algorithms.

    Global minimum: xi = 20
    Bounds: [-100, 100] for each dimension practically

    :param parameters: vector of parameters to function (one per dimension)
    :type parameters: list(float)
    :return: result of executing function
    :rtype: float
    """

    return sum([(xi - 20) ** 2 for xi in parameters])


def create_sphere(num_genes, threshold):
    """
    Create instance of Shifted Sphere function appropriately wrapped by Fitness class for benchmarking
    :param num_genes: number of dimensions in search space
    :type num_genes: int
    :param threshold: acceptable distance from minimum
    :return: a wrapped fitness function for GA consumption
    :rtype: Fitness
    """

    minimum = -100.0
    maximum = 100.0
    bounds = build_bounds(num_genes, minimum, maximum)
    optimum = [20.0 for _ in range(0, num_genes)]

    return Fitness(bounds, optimum, threshold, sphere)


def rosenbrock(chromosome):
    """
    Implementation of the n dimensional Rosenbrock function for benchmarking genetic algorithms.

    Global minimum: xi = 1
    Bounds: [-100, 100]

    :param chromosome: vector of parameters to function (one per dimension)
    :type chromosome: list(float)
    :return: result of executing function
    :rtype: float
    """

    fitness = 0
    for i in range(0, len(chromosome) / 2):
        fitness += 100 * (chromosome[2 * i + 1] - chromosome[2 * i]**2)**2 + (chromosome[2 * i - 1] - 1)**2

    return fitness


def create_rosenbrock(num_genes, threshold):
    """
    Create instance of Rosenbrock function appropriately wrapped by Fitness class for benchmarking
    :param num_genes: number of dimensions in search space
    :type num_genes: int
    :param threshold: acceptable distance from minimum
    :return: a wrapped fitness function for GA consumption
    :rtype: Fitness
    """

    minimum = -20.0
    maximum = 20.0
    bounds = build_bounds(num_genes, minimum, maximum)
    optimum = [1.0 for _ in range(0, num_genes)]

    return Fitness(bounds, optimum, threshold, rosenbrock)


def ackley(chromosome):
    """
    Implementation of the n dimensional Ackley function for benchmarking genetic algorithms.

    Global minimum: 0.0
    Bounds: [-32.768.0, 32.768]

    :param chromosome: vector of parameters to function (one per dimension)
    :type chromosome: list(float)
    :return: result of executing function
    :rtype: float
    """

    divisor = 1.0 / len(chromosome)
    first_exp = math.exp(-0.2 * math.sqrt(divisor * sum([gene ** 2 for gene in chromosome])))
    second_exp = math.exp(divisor * sum([math.cos(2.0 * math.pi * gene) for gene in chromosome]))

    return -20.0 * first_exp - second_exp + 20.0 + math.exp(1)


def create_ackley(num_genes, threshold):
    """
    Create instance of Ackley function appropriately wrapped by Fitness class for benchmarking
    :param num_genes: number of dimensions in search space
    :type num_genes: int
    :param threshold: acceptable distance from minimum
    :return: a wrapped fitness function for GA consumption
    :rtype: Fitness
    """

    minimum = -32.768
    maximum = 32.768
    bounds = build_bounds(num_genes, minimum, maximum)
    optimum = [0.0 for _ in range(0, num_genes)]

    return Fitness(bounds, optimum, threshold, ackley)


def rastrigin(parameters):
    """
    Implementation of n dimensional rastrigin function for benchmarking genetic algorithms

    Global optimum: xi = 0.0
    Bounds = [-6, 6] ...

    :param parameters:
    :type parameters: list(float)
    :return: result of executing function
    :rtype: float
    """

    magnitude = 10
    bound = 5.12

    term = 0
    for gene in parameters:
        if gene < - bound or gene > bound:
            term += magnitude * gene ** 2
        else:
            term += gene ** 2 - magnitude * math.cos(2 * math.pi * gene)

    return magnitude * len(parameters) + term


def create_rastrigin(num_genes, threshold):
    """
    Create wrapped Rastrigin
    :param num_genes: number of dimensions to function
    :type num_genes: int
    :param threshold: distance from optimum at which GA is said to have found optimum
    :type threshold: float
    :return: a wrapped fitness function for GA consumption
    :rtype: Fitness
    """

    minimum = -6.0
    maximum = 6.0
    bounds = build_bounds(num_genes, minimum, maximum)
    optimum = [0.0 for _ in range(0, num_genes)]

    return Fitness(bounds, optimum, threshold, rastrigin)


def schwefel(chromosome):
    """
    Implementation of Schwefel function for benchmarking genetic algorithms

    Bounds: may be infinite
    Optimum: xi = 420.9867

    :param chromosome: vector of parameters to function (one per dimension)
    :type chromosome: list(float)
    :return: result of executing function
    :rtype: float
    """

    magnitude = 418.9829        # Not really magnitude really a shift of minimum
    bound = 500.0

    term = 0
    for gene in chromosome:
        if gene < - bound or gene > bound:
            term += 0.02 * gene ** 2
        else:
            term += - gene * math.sin(math.sqrt(abs(gene)))

    return magnitude * len(chromosome) + term


def create_schwefel(num_genes, threshold):
    """
    Create wrapped Schwefel function
    :param num_genes: number of dimensions to function
    :type num_genes: int
    :param threshold: distance from optimum at which GA is said to have found optimum
    :type threshold: float
    :return: a wrapped fitness function for GA consumption
    :rtype: Fitness
    """

    minimum = -550.0
    maximum = 550.0
    optimum_coord = 420.9687

    bounds = build_bounds(num_genes, minimum, maximum)
    optimum = [optimum_coord for _ in range(0, num_genes)]

    return Fitness(bounds, optimum, threshold, schwefel)


def schaffer(chromosome):
    """
    Implementation of Schaffer's F7 function for benchmarking genetic algorithms.
    Requires at least two inputs

    Bounds xi =[-100, 100]
    Optimum xi = 0

    :param chromosome: vector of parameters to function (one per dimension)
    :type chromosome: list(float)
    :return: result of executing function
    :rtype: float
    """

    normalizer = 1.0 / (len(chromosome) - 1)
    fitness = 0
    for i in range(0, len(chromosome) - 1):
        si = math.sqrt(chromosome[i] ** 2 + chromosome[i + 1] ** 2)
        inner = normalizer * math.sqrt(si) * (math.sin(50.0 * math.pow(si, 0.2)) + 1)
        fitness += math.pow(inner, 2.0)

    return fitness


def create_schaffer(num_genes, threshold):
    """
    Create wrapped Schwefel function
    :param num_genes: number of dimensions to function
    :type num_genes: int
    :param threshold: distance from optimum at which GA is said to have found optimum
    :type threshold: float
    :return: a wrapped fitness function for GA consumption
    :rtype: Fitness
    """

    minimum = -100.0
    maximum = 100.0

    bounds = build_bounds(num_genes, minimum, maximum)
    optimum = [0.0 for _ in range(0, num_genes)]

    return Fitness(bounds, optimum, threshold, schaffer)


def test_bounds():

    bounds = Bounds()
    bounds.add_bound(0, 1, 10)
    bounds.add_bound(1, 1, 10)

    minimum, maximum = bounds[1]

    if minimum != 1 or maximum != 10:
        print 'bounds not functioning'


def test_fitness():

    bounds = Bounds()
    bounds.add_bound(0, -10, 10)
    bounds.add_bound(1, 5, 10)
    bounds.add_bound(2, 0, 10)

    optimum = [-10, 5, 0]
    threshold = 1

    fitness = Fitness(bounds, optimum, threshold, test)

    if fitness.calculate(optimum) != -5:
        print 'fitness not being calculated correctly'

    if not fitness.reached(optimum):
        print 'check for convergence to optimum not working'

    if fitness.reached([0, 0, 0]):
        print 'check for convergence to optimum not working'
