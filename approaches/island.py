from material import Iterator, Population
import random
import copy


class Island(Iterator):

    """
    Strategy is to force the population to have at least x amount of diversity.

    Separate diversity into islands, if diversity on an island drops too far cross pollinate
    with other individuals, regardless cross pollinate every x generations

    General island procedure with diversity twist.
    """

    def __init__(self, fitness, islands, pollination_interval, pollination_ratio, diversity_threshold, repl_rate,
                 mut_rate, tourn_favor, alpha, elitism, recording_interval):
        """

        :param fitness: wrapped fitness function with bounds information
        :type fitness: Fitness
        :param islands: number of islands to maintain in the population
        :type islands: int
        :param pollination_interval: max number of gens between cross pollination
        :type pollination_interval: int
        :param pollination_ratio: percentage of individuals to cross pollinate
        :type pollination_ratio: float
        :param diversity_threshold: the minimum diversity for exploitation mode
        :type diversity_threshold: float
        :param repl_rate: rate at which individuals die off
        :type repl_rate: float
        :param mut_rate: mutation rate when in mutation phase
        :type mut_rate: float
        :param alpha: percentage outside interval to consider
        :type alpha: float
        :param elitism: number of individuals that always survive
        :type elitism: int
        :param recording_interval: number of individuals guaranteed to survive (best x)
        :type recording_interval: int
        """
        Iterator.__init__(self, fitness, repl_rate=repl_rate, mut_rate=mut_rate, tourn_favor=tourn_favor,
                          alpha=alpha, elitism=elitism, recording_interval=recording_interval)
        self.islands = islands
        self.pollination_interval = pollination_interval
        self.pollination_ratio = pollination_ratio
        self.diversity_threshold = diversity_threshold
        self.gens_since_pollination = [0 for _ in range(0, islands)]
        self.cross_pollinations = 0

    def pre_hook(self, population, report):

        report.record['cross_pollinations'] = list()

        # Initialize population to random islands
        for i in range(0, len(population.individuals)):
            population.individuals[i].island = i % self.islands

    def pre_iter_hook(self, population, report):

        if not population.gen % self.recording_interval:
            report.record['cross_pollinations'].append((population.gen, self.cross_pollinations))

    def post_hook(self, population, report):

        report.final['corss_pollinations'] = self.cross_pollinations

    def exploitation_mode(self, population):
        return Iterator.diversity(population, self.diagonal) > self.diversity_threshold

    def iterate(self, population):
        """
        Given a population perform single iteration of operations
        :param population: population to perform strategy on
        :type population: Population
        :return: completed population
        :rtype: Population
        """
        pop_size = len(population.individuals)
        new_population = list()

        islands = [[] for _ in range(0, self.islands)]

        # Split population into islands then
        for i in range(0, len(population.individuals)):

            # Get number of island and assign individual
            ind = population.individuals[i]
            islands[ind.island].append(ind)

        for island_num in range(0, len(islands)):

            island = islands[island_num]
            island_size = len(island)
            propagated = 0

            survivors = self.tournament_selection(Population(population.gen, island))
            new_population.extend(self.mutate(survivors, use_elitism=True))

            propagated += len(survivors)

            # If diversity has dropped too low, or cross pollination has not
            # occurred in too many generations, do cross pollination
            if not self.exploitation_mode(Population(population.gen, island))\
                    or self.pollination_interval <= self.gens_since_pollination[island_num]:

                # Reset gens since pollination
                self.gens_since_pollination[island_num] = 0

                # Do a primitive cross pollination by selecting random individuals
                # from other populations to cross pollinate
                cross_pollinate = int(self.pollination_ratio * island_size)
                for _ in range(0, cross_pollinate):

                    from_island = random.randint(0, self.islands - 1)
                    ind_from_island = random.randint(0, island_size - 1)

                    duplicate = copy.deepcopy(islands[from_island][ind_from_island])
                    duplicate.island = island_num
                    new_population.append(duplicate)
                    propagated += 1
                    self.cross_pollinations += 1

            else:
                self.gens_since_pollination[island_num] += 1

            crosses = list()
            generate = island_size - propagated

            while len(crosses) < generate:

                first = random.randint(0, island_size - 1)
                second = random.randint(0, island_size - 1)

                if first == second:
                    continue

                cross1, cross2 = self.cross_single_gene(island[first], island[second])
                crosses.append(cross1)

                if len(crosses) < generate:
                    crosses.append(cross2)

            new_population.extend(self.mutate(crosses, use_elitism=False))

        for i in range(0, pop_size):

            if new_population[i].fitness is None:
                new_population[i].fitness = self.fitness.calculate(new_population[i].genes)
                self.fitness_applications += 1

        new_population.sort(key=lambda x: x.fitness, reverse=False)

        return new_population
