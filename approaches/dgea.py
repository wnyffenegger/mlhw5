from material import Iterator
from benchmark import distance
import random


class DGEA(Iterator):

    """
    Strategy is to force the population to have at least x amount of diversity.

    If diversity is high enough operate in exploitation mode (selection and crossover)

    If diversity is too low mutate the current population until diversity is high enough.

    From Diversity Guided Evolutionary Algorithms paper appearing in Springer
    """

    def __init__(self, fitness, expl_threshold, repl_rate, mut_rate, tourn_favor, alpha, elitism, recording_interval):
        """

        :param fitness: wrapped fitness function with bounds information
        :type fitness: Fitness
        :param expl_threshold: the minimum diversity for exploitation mode
        :type expl_threshold: float
        :param repl_rate: rate at which individuals die off
        :type repl_rate: float
        :param mut_rate: mutation rate when in mutation phase
        :type mut_rate: float
        :param alpha: percentage outside interval to consider
        :type alpha: float
        :param elitism: number of individuals that always survive
        :type elitism: int
        :param recording_interval: number of individuals guaranteed to survive (best x)
        :type recording_interval: int
        """
        Iterator.__init__(self, fitness, repl_rate=repl_rate, mut_rate=mut_rate, tourn_favor=tourn_favor,
                          alpha=alpha, elitism=elitism, recording_interval=recording_interval)
        self.exploitation_threshold = expl_threshold

    def exploitation_mode(self, population):
        return Iterator.diversity(population, self.diagonal) > self.exploitation_threshold

    def iterate(self, population):
        """
        Given a population perform single iteration of operations
        :param population: population to perform strategy on
        :type population: Population
        :return: completed population
        :rtype: Population
        """
        pop_size = len(population.individuals)
        new_population = list()

        if self.exploitation_mode(population):

            # Conduct normal selection and crossover
            # with no mutation

            survivors = self.tournament_selection(population)
            crosses = list()

            generate = int(self.repl_rate * pop_size)

            # Generate crosses while there remain crosses to generate
            while len(crosses) < generate:

                first = random.randint(0, pop_size - 1)
                second = random.randint(0, pop_size - 1)

                if first == second:
                    continue

                # cross a single gene
                # crossing all genes with blx-alpha is essentially a mutation
                cross1, cross2 = self.cross_single_gene(population.individuals[first], population.individuals[second])
                crosses.append(cross1)
                crosses.append(cross2)

            new_population.extend(survivors)
            new_population.extend(crosses)

        else:
            # Conduct mutation on self.rate percent of the individuals in the population
            new_population.extend(self.mutate(population.individuals, use_elitism=True))

        for i in range(0, pop_size):

            if new_population[i].fitness is None:
                new_population[i].fitness = self.fitness.calculate(new_population[i].genes)
                self.fitness_applications += 1

        new_population.sort(key=lambda x: x.fitness, reverse=False)

        return new_population
