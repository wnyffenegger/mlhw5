from material import Iterator
from benchmark import distance
import random


class Artificial(Iterator):

    """
    Strategy is to force the population to maintain diversity through inserting artificial
    individuals.

    Always select, crossover, and mutate

    If diversity is too low generate artificial individuals not similar to any individuals in population
    and insert into the population in lieu of some crossover individuals
    """

    def __init__(self, fitness, diversity_threshold, artificial_threshold, artificial_ratio, repl_rate,
                 mut_rate, tourn_favor, alpha, elitism, recording_interval):
        """

        :param fitness: wrapped fitness function with bounds information
        :type fitness: benchmark.Fitness
        :param diversity_threshold: the minimum diversity for exploitation mode
        :type diversity_threshold: float
        :param artificial_threshold: the minimum distance from every individual
        for an artificial individual to introduce new material
        :type artificial_threshold: float
        :param artificial_ratio: percentage of individuals to replace in population if diversity is too low
        :type artificial_ratio: float
        :param repl_rate: rate at which individuals die off
        :type repl_rate: float
        :param mut_rate: mutation rate when in mutation phase
        :type mut_rate: float
        :param alpha: percentage outside interval to consider
        :type alpha: float
        :param elitism: number of individuals that always survive
        :type elitism: int
        :param recording_interval: number of individuals guaranteed to survive (best x)
        :type recording_interval: int
        """
        Iterator.__init__(self, fitness, repl_rate=repl_rate, mut_rate=mut_rate, tourn_favor=tourn_favor,
                          alpha=alpha, elitism=elitism, recording_interval=recording_interval)
        self.diversity_threshold = diversity_threshold
        self.artifical_threshold = artificial_threshold
        self.artificial_ratio = artificial_ratio
        self.artificial_individuals = 0

    def pre_hook(self, population, report):

        # Create tracking mechanism for aritifical individuals
        report.record['artificial_individuals'] = list()

    def post_iter_hook(self, population, report):

        if not population.gen % self.recording_interval:
            report.record['artificial_individuals'].append((population.gen, self.artificial_individuals))

    def post_hook(self, population, report):

        report.final['artificial_individuals'] = self.artificial_individuals

    def low_diversity(self, population):
        return Iterator.diversity(population, self.diagonal) < self.diversity_threshold

    def iterate(self, population):
        """
        Given a population perform single iteration of operations
        :param population: population to perform strategy on
        :type population: Population
        :return: completed population
        :rtype: Population
        """
        pop_size = len(population.individuals)
        new_population = list()

        propagated = 0

        # Conduct normal selection
        survivors = self.tournament_selection(population)
        propagated += len(survivors)
        new_population.extend(self.mutate(survivors, use_elitism=True))

        if self.low_diversity(population):

            generate = self.artificial_ratio * pop_size
            artificial = list()

            while len(artificial) < generate:

                individual = self.generate_individual()
                distinct = True
                for ind in population.individuals:

                    if self.artifical_threshold > distance(individual.genes, ind.genes):
                        distinct = False
                        break

                if distinct:
                    artificial.append(individual)
                    propagated += 1
                    self.artificial_individuals += 1

            new_population.extend(artificial)

        # Conduct normal crossover
        crosses = list()

        generate = pop_size - propagated

        # Generate crosses while there remain crosses to generate
        while len(crosses) < generate:

            first = random.randint(0, pop_size - 1)
            second = random.randint(0, pop_size - 1)

            if first == second:
                continue

            # cross a single gene
            # crossing all genes with blx-alpha is essentially a mutation
            cross1, cross2 = self.cross_single_gene(population.individuals[first], population.individuals[second])
            crosses.append(cross1)

            if len(crosses) < generate:
                crosses.append(cross2)

        new_population.extend(self.mutate(crosses, use_elitism=False))

        for i in range(0, pop_size):

            if new_population[i].fitness is None:
                new_population[i].fitness = self.fitness.calculate(new_population[i].genes)
                self.fitness_applications += 1

        new_population.sort(key=lambda x: x.fitness, reverse=False)

        return new_population
