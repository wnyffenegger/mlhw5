from benchmark import distance
from material import Iterator, Individual, Population
import random
import math

REGULAR_IND = 1
HOF_IND = 2


class Dichotomy(Iterator):

    """
    Strategy is to force the population to have at least x amount of diversity.

    If diversity is high enough operate in exploitation mode (selection and crossover)

    If diversity is too low mutate the current population until diversity is high enough.

    From Diversity Guided Evolutionary Algorithms paper appearing in Springer
    """

    def __init__(self, fitness, focus, hof_size, repl_rate, mut_rate, tourn_favor, alpha, elitism, recording_interval):
        """

        :param fitness: wrapped fitness function with bounds information
        :type fitness: Fitness
        :param focus: largest possible mutation of a hall of fame individual as percentage of parameter size
        (maximum value of parameter - minimum value of parameter)
        :type focus: float
        :param hof_size: number of individuals in hof
        :type hof_size: int
        :param repl_rate: rate at which individuals die off
        :type repl_rate: float
        :param mut_rate: mutation rate when in mutation phase
        :type mut_rate: float
        :param alpha: percentage outside interval to consider
        :type alpha: float
        :param elitism: number of individuals that always survive
        :type elitism: int
        :param recording_interval: number of individuals guaranteed to survive (best x)
        :type recording_interval: int
        """
        Iterator.__init__(self, fitness, repl_rate=repl_rate, mut_rate=mut_rate,
                          tourn_favor=tourn_favor, alpha=alpha, elitism=elitism, recording_interval=recording_interval)
        self.focus = focus
        self.hof_size = hof_size
        self.dissimilarity = math.sqrt(sum((focus * (maximum - minimum)) ** 2
                                           for minimum, maximum in fitness.bounds.values()))
        self.dissimilar_calcs = 0

    def pre_hook(self, population, report):
        """
        Assign hof_size individuals to hall of fame to start with. Choose first hof_size distinct individuals
        :param population: initial population of individuals
        :type population: Population
        :param report: search report
        :type report: SearchReport
        :return:
        """

        # Add dissimilarity tracking
        report.record['dissimilarity'] = list()

        # Set initial hall of fame
        hof = []
        for ind in population.individuals:
            if self.dissimilar(hof, ind):
                ind.island = HOF_IND
                hof.append(ind)

                if len(hof) >= self.hof_size:
                    break

        for ind in population.individuals:
            if ind.island is None:
                ind.island = REGULAR_IND

    def pre_iter_hook(self, population, report):

        # Add record of dissimilarity calculations
        if not population.gen % self.recording_interval:
            report.record['dissimilarity'].append((population.gen, self.dissimilar_calcs))

    def post_hook(self, population, report):

        report.final['dissimilarity'] = self.dissimilar_calcs

    def mutate_limited(self, ind):
        """
        Mutate a single gene in an individual limited to a mutation close to the current value of the gene
        :param ind: the individual to mutate
        :type ind: Individual
        :return: a new individual that has been mutated
        :rtype: Individual
        """

        # Get random gene to mutate and find min/max bounds for mutation
        new_chromosome = list()

        for gene_num in range(0, len(ind.genes)):
            minimum, maximum = self.fitness.bounds[gene_num]

            new_gene = ind.genes[gene_num]
            max_delta = self.focus * (maximum - minimum)
            delta = random.random() * max_delta

            if random.randint(0, 1):
                new_gene = new_gene + delta
            else:
                new_gene = new_gene - delta

            if new_gene > maximum:
                new_gene = maximum
            elif new_gene < minimum:
                new_gene = minimum

            new_chromosome.append(new_gene)

        self.mutations += 1

        # Make sure mutant belongs to same island as individual
        mutant = Individual(ind.uid, new_chromosome)
        if ind.island is not None:
            mutant.island = ind.island

        return mutant

    def dissimilar(self, hof, ind):
        """
        Test whether individual is dissimilar from every hof individual
        :param hof: list of best individuals which must be dissimilar
        :type hof: list(Individual)
        :param ind: individual to test against hof
        :type ind: Individual
        :return: True if dissimilar from every individual
        :rtype: bool
        """

        self.dissimilar_calcs += 1

        for hof_ind in hof:
            if distance(hof_ind.genes, ind.genes) < self.dissimilarity:
                return False

        return True

    def iterate(self, population):
        """
        Given a population perform single iteration of operations
        :param population: population to perform strategy on
        :type population: Population
        :return: completed population
        :rtype: Population
        """

        pop_size = len(population.individuals)
        new_population = list()

        # Split into active population (selection, crossover) and mutation population
        live = []
        hof = []
        for ind in population.individuals:
            if ind.island == 2:
                hof.append(ind)

        # Update hof
        for ind in population.individuals:

            # If a survivor or crossover individual
            if ind.island == REGULAR_IND:
                worst_hof = hof[-1]

                # Don't bother doing checks
                if worst_hof.fitness < ind.fitness:
                    live.append(ind)
                    continue

                if self.dissimilar(hof, ind):

                    # Swap individuals
                    ind.island = HOF_IND
                    hof[-1] = ind

                    hof.sort(key=lambda x: x.fitness)
                else:
                    self.dissimilar_calcs += 1
                    replaced = False
                    matches = 0
                    for i in range(0, len(hof)):
                        if distance(hof[i].genes, ind.genes) < self.dissimilarity and hof[i].fitness > ind.fitness:
                            matches += 1
                            hof[i] = ind
                            ind.island = HOF_IND
                            replaced = True

                    if not replaced:
                        live.append(ind)

        # Only select survivors from the list of active individuals
        survivors = self.tournament_selection(Population(population.gen, live))
        propagated = len(survivors)
        new_population.extend(self.mutate(survivors, use_elitism=True))

        # We may steal individuals from regular pool and place into hall of fame
        generate = pop_size - propagated - len(hof)
        crosses = list()

        # Generate crosses while there remain crosses to generate
        while len(crosses) < generate:

            first = random.randint(0, pop_size - 1)
            second = random.randint(0, pop_size - 1)

            if first == second:
                continue

            # cross a single gene
            # crossing all genes with blx-alpha is essentially a mutation
            cross1, cross2 = self.cross_single_gene(population.individuals[first], population.individuals[second])

            cross1.island = REGULAR_IND
            crosses.append(cross1)

            if len(crosses) < generate:
                cross2.island = REGULAR_IND
                crosses.append(cross2)

        new_population.extend(self.mutate(crosses, use_elitism=False))

        # Attempt to improve hof by mutating each individual and testing the
        # mutation against current individual, if improvement take mutation.
        # Mutation is limited to area around individual
        for ind in hof:
            attempted_improvement = self.mutate_one(ind)
            attempted_improvement.fitness = self.fitness.calculate(attempted_improvement.genes)
            self.fitness_applications += 1

            if attempted_improvement.fitness < ind.fitness:
                new_population.append(attempted_improvement)
            else:
                new_population.append(ind)

        for i in range(0, pop_size):

            if new_population[i].fitness is None:
                new_population[i].fitness = self.fitness.calculate(new_population[i].genes)
                self.fitness_applications += 1

        new_population.sort(key=lambda x: x.fitness, reverse=False)

        return new_population
