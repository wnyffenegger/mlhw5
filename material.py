from benchmark import distance, test, Bounds, Fitness
import copy
import random
import time
from math import sqrt

CROSSOVERS = 'crossovers'
DISTANCE = 'distance'
FITNESS_CALC = 'fitness_calculations'
FITNESS_RECORD = 'fitness_record'
INDIVIDUALS_CREATED = 'individuals_created'
MUTATIONS = 'mutations'
SELECTIONS = 'selections'

class Individual:

    def __init__(self, uid, genes):
        """
        Create individual with list of genes
        :param uid: unique id of individual
        :type uid: str | int
        :param genes: list of function parameters
        :type genes: list(float)
        """

        self.uid = uid

        # Copy genes just for insurance
        self.genes = copy.deepcopy(genes)
        self.fitness = None
        self.island = None

    def similarity(self, individual):
        """
        Find euclidian distance between individuals
        :param individual: individual to compare against
        :type individual: Individual
        :return: Euclidian distance between individuals
        :rtype: float
        """

        if len(self.genes) != len(individual.genes):
            print 'calculating similarity of individuals with different length genes'

        return distance(self.genes, individual.genes)

    def __str__(self):
        return str(self.uid) + ': ' + ', '.join(['%0.3f' % g for g in self.genes])


class Population:

    def __init__(self, gen, individuals=None):
        """

        :param gen: generation of population
        :type gen: int
        :param individuals: list of individuals in population
        :type individuals: list(Individual)
        """

        self.gen = gen

        if individuals is not None:
            self.individuals = individuals
        else:
            self.individuals = list()

    def add(self, individual):
        self.individuals.append(individual)


class Iterator:
    """
    Class where search strategy is implemented

    Crossover, mutation, and selection operators are all given to standardize between strategies.
    """

    def __init__(self, fitness, repl_rate, mut_rate, tourn_favor, alpha, elitism, recording_interval):
        """
        Set parameters for iterator
        :param fitness: set of bounds for each fitness function, limits crossover operator to those bounds
        :type fitness: Fitness
        :param repl_rate: percent of individuals that die off
        :type repl_rate: float
        :param mut_rate: rate at which individuals are randomly mutated
        :type mut_rate: float
        :param tourn_favor: likelihood of tournament winner surviving
        :type tourn_favor: float
        :param alpha: percentage of interval that crossover can lead to values outside of
        :type alpha: float
        :param elitism: number of individuals guaranteed to survive (best x)
        :type elitism: int
        :param recording_interval: number of individuals guaranteed to survive (best x)
        :type recording_interval: int
        """

        self.fitness = fitness
        self.repl_rate = repl_rate
        self.mut_rate = mut_rate
        self.tourn_favor = tourn_favor
        self.alpha = alpha
        self.elitism = elitism

        self.mutations = 0
        self.crossovers = 0
        self.selections = 0
        self.fitness_applications = 0

        self.total_inds = 0

        self.recording_interval = recording_interval

        self.diagonal = sqrt(sum([pow(maximum - minimum, 2) for minimum, maximum in self.fitness.bounds.values()]))

    def pre_hook(self, population, report):
        """
        Perform necessary pre-search modifications on data
        :param population: starting population
        :type population: Population
        :param report: search report
        :type report: SearchReport
        """
        pass

    def post_hook(self, population, report):
        """
        Perform necessary post-search modifications on data
        :param population: starting population
        :type population: Population
        :param report: search report
        :type report: SearchReport
        """
        pass

    def pre_iter_hook(self, population, report):
        """
        Perform necessary pre-iteration modifications on data
        :param population: starting population
        :type population: Population
        :param report: search report
        :type report: SearchReport
        """
        pass

    def post_iter_hook(self, population, report):
        """
        Perform necessary post-iteration modifications on data
        :param population: starting population
        :type population: Population
        :param report: search report
        :type report: SearchReport
        """
        pass

    def generate_population(self, pop_size, gen=0):
        """
        Generate random population using bounds of provided Fitness function
        :param pop_size: number of individuals in population
        :type pop_size: int
        :param gen: generation to label population as
        :type gen: int
        :return: population of randomly generated individuals
        :rtype: Population
        """

        individuals = list()
        for i in range(0, pop_size):

            individuals.append(self.generate_individual())

        for ind in individuals:
            ind.fitness = self.fitness.calculate(ind.genes)

        individuals.sort(key=lambda x: x.fitness)
        return Population(gen, individuals)

    def generate_individual(self):
        """
        Generate random individual using bounds of provided Fitness function
        :return: randomly generated individual
        :rtype: list(float)
        """

        chromosome = []
        for j in range(0, len(self.fitness.bounds.keys())):
            minimum, maximum = self.fitness.bounds[j]

            gene = minimum + (maximum - minimum) * random.random()
            chromosome.append(gene)

        self.total_inds += 1
        return Individual(self.total_inds, chromosome)

    def iterate(self, population):
        """
        Conduct some combination of selection, crossover, and mutation operators
        :param population:
        :return: list of individuals forming new population
        :rtype: Population
        """
        pass

    def tournament_selection(self, population):
        """
        Given a population select the most fit individuals proportionally to their fitness
        :param population: the population to select from
        :type population: Population
        :return: list of individuals selected to survive
        :rtype: list(Individual)
        """

        # Set up for selection
        pop_size = len(population.individuals)
        inds = population.individuals

        # Number of survivors
        num_survivors = int(pop_size - self.repl_rate * pop_size)

        # Indices of selected individuals
        # Always pre-select best x individuals for survival
        selected = [i for i in range(0, self.elitism)]

        # Select from all individuals
        remaining = [i for i in range(self.elitism, pop_size)]
        while len(selected) < num_survivors:
            first = random.choice(remaining)
            second = random.choice(remaining)

            # Don't compete with self
            if first == second:
                continue

            winner = first
            loser = second

            # Swap if necessary
            if inds[first].fitness > inds[second].fitness:
                loser = first
                winner = second

            # With bias towards winner randomly select individuals to survive
            r = random.random()
            if r < self.tourn_favor:
                selected.append(winner)
                remaining.remove(winner)
            else:
                selected.append(loser)
                remaining.remove(loser)

            self.selections += 1

        # Convert indices to individuals
        return [inds[i] for i in selected]

    def mutate_one(self, ind):
        """
        Mutate a single gene in the individual with uniform randomness. There are better mutators, but this is
        for a benchmarking test so.
        :param ind: the individual to mutate
        :type ind: Individual
        :return: a new individual that has been mutated
        :rtype: Individual
        """

        # Get random gene to mutate and find min/max bounds for mutation
        gene_num = random.randint(0, len(ind.genes) - 1)
        minimum, maximum = self.fitness.bounds[gene_num]

        new_gene = ind.genes[gene_num]

        if random.randint(0, 1):
            new_gene = new_gene + random.random() * (maximum - new_gene)
        else:
            new_gene = new_gene - random.random() * (new_gene - minimum)

        new_chromosome = list()
        for i in range(0, len(ind.genes)):
            if i != gene_num:
                new_chromosome.append(ind.genes[i])
            else:
                new_chromosome.append(new_gene)

        self.mutations += 1

        # Make sure mutant belongs to same island as individual
        mutant = Individual(ind.uid, new_chromosome)
        if ind.island is not None:
            mutant.island = ind.island

        return mutant

    def mutate_all(self, ind):
        """
        Mutate a single gene in the individual with uniform randomness. There are better mutators, but this is
        for a benchmarking test so.
        :param ind: the individual to mutate
        :type ind: Individual
        :return: a new individual that has been mutated
        :rtype: Individual
        """

        # Get random gene to mutate and find min/max bounds for mutation

        new_chromosome = list()
        for i in range(0, len(ind.genes)):
            minimum, maximum = self.fitness.bounds[i]
            new_gene = ind.genes[i]

            if random.randint(0, 1):
                new_gene = new_gene + random.random() * (maximum - new_gene)
            else:
                new_gene = new_gene - random.random() * (new_gene - minimum)

            new_chromosome.append(new_gene)

        self.mutations += 1

        # Make sure mutant belongs to same island
        mutant = Individual(ind.uid, new_chromosome)
        if ind.island is not None:
            mutant.island = ind.island

        return mutant

    def mutate(self, individuals, use_elitism, mutate_chromosome=False):
        """
        Mutate all individuals preserving elite individuals
        :param individuals: list of individuals that may be mutated
        :type individuals: list(Individuals)
        :param use_elitism: if True preserve best x individuals provided otherwise legally mutate them
        :type use_elitism: bool
        :param mutate_chromosome: if True mutate every gene in chromosome when doing mutation
        :type mutate_chromosome: bool
        :return: list of individuals mutated or unchanged
        :rtype: list(Individual)
        """

        # preserve without mutation x individuals

        mutated = list()
        if use_elitism:
            mutated.extend(individuals[:self.elitism])

        possible = individuals
        if use_elitism:
            possible = possible[self.elitism:]

        for ind in possible:
            r = random.random()

            if r < self.mut_rate:
                if mutate_chromosome:
                    mutated.append(self.mutate_all(ind))
                else:
                    mutated.append(self.mutate_one(ind))
            else:
                mutated.append(ind)

        return mutated

    def cross_genes(self, gene_num, gene1, gene2):
        """
        Cross two genes and return a single potential gene
        :param gene_num:
        :type gene_num: int
        :param gene1:
        :type gene1: float
        :param gene2:
        :type gene2: float
        :return: a crossed gene
        :rtype: float
        """

        lower_bound, upper_bound = self.fitness.bounds[gene_num]
        interval = abs(gene2 - gene1)

        if gene1 < gene2:
            lower = gene1
            upper = gene2
        else:
            lower = gene2
            upper = gene1

        minimum = lower - self.alpha * interval
        if minimum < lower_bound:
            minimum = lower_bound

        maximum = upper + self.alpha * interval
        if maximum > upper_bound:
            maximum = upper_bound

        return random.uniform(minimum, maximum)

    def cross_single_gene(self, ind1, ind2):
        """
        BLX-Alpha crossover implementation from Eshelman 1993 real coded GAs. Cross a single gene in an individual

        :param ind1:
        :type ind1: Individual
        :param ind2:
        :type ind2: Individual
        :return: two new individuals
        :rtype: Individual, Individual
        """

        gene_to_cross = random.randint(0, len(ind1.genes) - 1)

        crossed_gene1 = self.cross_genes(gene_to_cross, ind1.genes[gene_to_cross], ind2.genes[gene_to_cross])
        crossed_gene2 = self.cross_genes(gene_to_cross, ind1.genes[gene_to_cross], ind2.genes[gene_to_cross])

        crossover1 = list()
        crossover2 = list()

        for i in range(0, len(ind1.genes)):
            if i != gene_to_cross:
                crossover1.append(ind1.genes[i])
                crossover2.append(ind2.genes[i])
            else:
                crossover1.append(crossed_gene1)
                crossover2.append(crossed_gene2)

        self.total_inds += 1
        cross1 = Individual(self.total_inds, crossover1)
        cross1.island = ind1.island

        self.total_inds += 1
        cross2 = Individual(self.total_inds, crossover2)
        cross2.island = ind2.island

        self.crossovers += 2

        return cross1, cross2

    def cross_all_genes(self, ind1, ind2):
        """
        BLX-Alpha crossover implementation from Eshelman 1993 real coded GAs. Cross every gene in individual
        :param ind1: an individual to crossover
        :type ind1: Individual
        :param ind2: an individual to crossover
        :type ind2: Individual
        :return: single new individual
        :rtype: Individual
        """

        crossed = list()

        for i in range(0, len(ind1.genes)):
            crossed.append(self.cross_genes(i, ind1.genes[i], ind2.genes[i]))

        self.total_inds += 1
        self.crossovers += 1

        cross = Individual(self.total_inds, crossed)
        cross.island = ind1.island

        return cross

    def met_criterion(self, population):
        """
        Test whether individuals in population (and potentially HOF) meet criterion
        :param population: a population to test
        :type population: Population
        :return: all individuals satisfying criterion
        :rtype list(Individual)
        """

        met = list()

        for ind in population.individuals:
            if self.fitness.reached(ind.genes):
                met.append(ind)

        return met

    def get_mutations(self):
        """
        :return: number of mutations conducted
        :rtype: int
        """
        return self.mutations

    def get_selections(self):
        """
        :return: number of selection operations conducted
        :rtype: int
        """
        return self.selections

    def get_crossovers(self):
        """
        :return: number of crossovers conducted
        :rtype: int
        """
        return self.crossovers

    def get_fitness_calculations(self):
        """
        :return: number of fitness function applications conducted
        :rtype: int
        """
        return self.fitness_applications

    @staticmethod
    def diversity(population, diagonal):
        """
        Calculate average distance to mean point as measure of diversity. Standardized so that each approach
        uses the same diversity measure

        :param population: population to find diversity of
        :type population: Population
        :param diagonal: maximum euclidean distance between two points in search space
        :type diagonal: float
        :return: average deviance from mean point of population
        :rtype: float
        """

        pop_size = len(population.individuals)
        chromosome_size = len(population.individuals[0].genes)

        # Get average point of population
        average = [0.0 for _ in range(0, chromosome_size)]
        for ind in population.individuals:
            for i in range(0, chromosome_size):
                average[i] += ind.genes[i]

        average = [a / pop_size for a in average]

        # Calculate average distance from point
        total_distance = 0.0
        for ind in population.individuals:
            total_distance += distance(average, ind.genes)

        return total_distance / (diagonal * pop_size)


class SearchReport:

    def __init__(self):
        """
        Summary of search
        """

        # List of various statistics recorded at check points during evaluation
        # each is a tuple (int, float) where the int is the generation the data point was taken at
        # and the float is the value
        self.record = {
            CROSSOVERS: list(),
            DISTANCE: list(),
            FITNESS_CALC: list(),
            FITNESS_RECORD: list(),
            INDIVIDUALS_CREATED: list(),
            MUTATIONS: list(),
            SELECTIONS: list()
        }

        self.final = {
            CROSSOVERS: 0,
            DISTANCE: 0.0,
            FITNESS_CALC: 0,
            FITNESS_RECORD: 0.0,
            INDIVIDUALS_CREATED: 0,
            MUTATIONS: 0,
            SELECTIONS: 0
        }

        self.best = None
        self.generations = 0
        self.time = 0
        self.special = dict()

    def increment_record(self, population, iterator):
        """
        Given an iterator pull out relevant data for incremental record
        :param population: instance of population to draw stats from
        :type population: Population
        :param iterator: iterator to draw stats from
        :type iterator: Iterator
        """

        self.record[CROSSOVERS].append((population.gen, iterator.crossovers))
        self.record[DISTANCE].append((population.gen,
                                      distance(population.individuals[0].genes, iterator.fitness.global_optimum)))
        self.record[FITNESS_CALC].append((population.gen, iterator.fitness_applications))
        self.record[FITNESS_RECORD].append((population.gen, population.individuals[0].fitness))
        self.record[INDIVIDUALS_CREATED].append((population.gen, iterator.total_inds))
        self.record[MUTATIONS].append((population.gen, iterator.mutations))
        self.record[SELECTIONS].append((population.gen, iterator.selections))

    def final_performance(self, population, iterator):
        """
        Given an iterator pull out relevant data for final statistics
        :param population: instance of population to draw stats from
        :type population: Population
        :param iterator: iterator to draw stats from
        :type iterator: Iterator
        """

        self.generations = population.gen
        self.best = population.individuals[0]

        self.final[CROSSOVERS] = iterator.crossovers
        self.final[DISTANCE] = distance(population.individuals[0].genes, iterator.fitness.global_optimum)
        self.final[FITNESS_CALC] = iterator.fitness_applications
        self.final[FITNESS_RECORD] = population.individuals[0].fitness
        self.final[INDIVIDUALS_CREATED] = iterator.total_inds
        self.final[MUTATIONS] = iterator.mutations
        self.final[SELECTIONS] = iterator.selections

    def __str__(self):
        printstr = 'Generations %d, Elapsed Time (ms): %d\n' % (self.generations, self.time)
        printstr += 'Best Individual: %s\n' % (', '.join(['%0.2f' % s for s in self.best.genes]))
        printstr += 'Fitness: %0.3f, Distance from Optimum %0.2f\n' % (self.best.fitness, self.final[DISTANCE])
        printstr += 'Fitness Calculations %d\n' % self.final[FITNESS_CALC]
        printstr += 'Selections Performed %d\n' % self.final[SELECTIONS]
        printstr += 'Crossover Individuals Generated %d\n' % self.final[CROSSOVERS]
        printstr += 'Mutations Performed %d\n' % self.final[MUTATIONS]

        for key, value in self.special.iteritems():
            printstr += '%s : %0.2f' % (key, value)

        return printstr + '\n'


class GeneticAlgorithm:

    def __init__(self, iterator, pop_size, iterations):
        """

        :param iterator: an iterator conducting selection crossover and mutation
        :type iterator: Iterator
        :param pop_size: number of individuals in each generation
        :type pop_size: int
        :param iterations:
        """

        self.iterator = iterator

        self.pop_size = pop_size
        self.max_iterations = iterations

        self.population = self.iterator.generate_population(pop_size)

        # Report on outcome of genetic search
        self.report = SearchReport()

    def iterate(self):
        """
        Conduct single iteration
        """

        if not self.population.gen % self.iterator.recording_interval:
            self.report.increment_record(self.population, self.iterator)
            print '%d fitness of %0.3f' % (self.population.gen, self.population.individuals[0].fitness)

        # Conduct pre iteration operations custom to algorithm implementation
        self.iterator.pre_iter_hook(self.population, self.report)

        pop = self.iterator.iterate(self.population)

        self.population = Population(self.population.gen + 1, pop)

        # Conduct post iteration operations custom to algorithm
        self.iterator.post_iter_hook(self.population, self.report)

    def run(self):
        """
        Iterate until solution found report
        :return: summary of search
        :rtype: SearchReport
        """

        start = int(round(time.time() * 1000))

        # Before search conduct any necessary arrangements for the algorithm
        self.iterator.pre_hook(self.population, self.report)

        iterations = 0
        while not len(self.iterator.met_criterion(self.population))\
                and self.population.gen < self.max_iterations:

            self.iterate()
            iterations += 1

        self.population.individuals.sort(key=lambda x: x.fitness, reverse=False)

        # Set final report statistics for search
        self.report.final_performance(self.population, self.iterator)
        self.report.time = int(round(time.time() * 1000)) - start

        self.iterator.post_hook(self.population, self.report)

        return self.report


def test_datatypes():

    ind1 = Individual(1, [3])
    ind2 = Individual(2, [2])

    if abs(ind2.similarity(ind1) - 1.0) > 0.0001:
        print 'similarity not calculated correctly'

    genes = [3, 4, 5]
    ind3 = Individual(3, genes)

    genes.remove(3)

    if len(ind3.genes) != 3:
        print 'individual should be immutable'

    pop = Population(0, [ind1, ind2])

    pop.add(ind3)

    if len(pop.individuals) != 3:
        print 'population is not recording individuals correctly'


def test_iterations():

    bounds = Bounds()
    bounds.add_bound(0, 0, 10)
    bounds.add_bound(1, 0, 10)

    fitness = Fitness(bounds, [0], 0.00001, test)

    # Test tournament selection
    test1 = Iterator(fitness, 0.5, 0.05, 0.85, 0.5, 0)

    ind1 = Individual(1, [1])
    ind2 = Individual(2, [2])

    ind1.fitness = 1
    ind2.fitness = 2

    pop = Population(0, [ind1, ind2])

    ind1_selected = 0
    for _ in range(0, 100):
        survivors = test1.tournament_selection(pop)

        if len(survivors) != 1:
            print 'should be one survivor only'

        if survivors[0].fitness == 1:
            ind1_selected += 1

    print 'Selection ratio %d:%d' % (ind1_selected, 100 - ind1_selected)

    # Test crossover

    for _ in range(0, 1000):
        cross = test1.cross_genes(0, ind1.genes[0], ind2.genes[0])

        if cross > 2.5 or cross < 0.5:
            print 'cross should not be outside BLX-Alpha bounds'

    ind3 = Individual(1, [0, 1])
    ind4 = Individual(1, [1, 2])

    for _ in range(0, 1000):
        cross = test1.cross_genes(0, ind3.genes[0], ind4.genes[0])

        if cross > 1.5 or cross < 0.0:
            print 'cross should not be outside BLX-Alpha bounds'

    cross1, cross2 = test1.cross_single_gene(ind3, ind4)
    cross = test1.cross_all_genes(ind3, ind4)

    print cross1
    print cross2
    print cross
