Name: Short 20D, Approach DGEA
Population Size: 400
Max Generations: 3000
Dimensions: 10
Replacement Rate: 0.90
Mutation Rate: 0.75
BLX-Alpha: 0.20
Elitism: 1
expl_threshold: 0.0250

Average Final Statistics
distance : 0.09
fitness_record : 0.15
selections : 17376.45
fitness_calculations : 163957.80
individuals_created : 160798.00
crossovers : 160398.00
mutations : 3559.80
