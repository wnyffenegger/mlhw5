Name: Islands 20D, Approach Islands
Population Size: 400
Max Generations: 3000
Dimensions: 10
Replacement Rate: 0.70
Mutation Rate: 0.05
BLX-Alpha: 0.20
Elitism: 1
pollination_ratio: 0.0500
islands: 4.0000
diversity_threshold: 0.0500
pollination_interval: 150.0000

Average Final Statistics
distance : 0.09
fitness_record : 0.17
selections : 30682.00
fitness_calculations : 71996.00
individuals_created : 71569.80
crossovers : 71169.80
mutations : 5078.45
corss_pollinations : 3612.75
