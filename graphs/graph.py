from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import benchmark


def test_sphere(file_path=None):
    """
    :param file_path: file to save function to
    :type file_path: str
    """

    fig = plt.figure()
    ax = plt.gca(projection='3d')

    x = np.arange(-100.0, 100.0, 1)
    y = np.arange(-100.0, 100.0, 1)

    X, Y = np.meshgrid(x, y)

    # https://stackoverflow.com/questions/8722735/i-want-to-use-matplotlib-to-make-a-3d-plot-given-a-z-function
    z = np.array([benchmark.sphere([x, y]) for x, y in zip(np.ravel(X), np.ravel(Y))])
    Z = z.reshape(X.shape)

    ax.plot_surface(X, Y, Z, cmap=plt.get_cmap('viridis'))

    if file_path is not None:
        print 'Saving to file %s' % file_path
        plt.savefig(file_path, dpi=300)
    else:
        plt.show()

    plt.close()


def test_rosenbrock(file_path=None):
    """
    :param file_path: file to save function to
    :type file_path: str
    """

    fig = plt.figure()
    ax = plt.gca(projection='3d')

    x = np.arange(-2, 2, 0.01)
    y = np.arange(-2, 2, 0.01)

    X, Y = np.meshgrid(x, y)

    # https://stackoverflow.com/questions/8722735/i-want-to-use-matplotlib-to-make-a-3d-plot-given-a-z-function
    z = np.array([benchmark.rosenbrock([x, y]) for x, y in zip(np.ravel(X), np.ravel(Y))])
    Z = z.reshape(X.shape)

    ax.view_init(30, 90)
    ax.plot_surface(X, Y, Z, cmap=plt.get_cmap('viridis'))

    if file_path is not None:
        print 'Saving to file %s' % file_path
        plt.savefig(file_path, dpi=300)
    else:
        plt.show()

    plt.close()


def test_ackley(file_path=None):
    """
    :param file_path: file to save function to
    :type file_path: str
    """

    fig = plt.figure()
    ax = plt.gca(projection='3d')

    # x = np.arange(-32.768, 32.768, 0.1)
    x = np.arange(-5, 5, 0.1)
    # y = np.arange(-32.768, 32.768, 0.1)
    y = np.arange(-5, 5, 0.1)

    X, Y = np.meshgrid(x, y)

    # https://stackoverflow.com/questions/8722735/i-want-to-use-matplotlib-to-make-a-3d-plot-given-a-z-function
    z = np.array([benchmark.ackley([x, y]) for x, y in zip(np.ravel(X), np.ravel(Y))])
    Z = z.reshape(X.shape)

    ax.plot_surface(X, Y, Z)

    if file_path is not None:
        print 'Saving to file %s' % file_path
        plt.savefig(file_path, dpi=300)
    else:
        plt.show()

    plt.close()


def test_rastrigin(file_path=None):
    """
    :param file_path: file to save function to
    :type file_path: str
    """

    fig = plt.figure()
    ax = plt.gca(projection='3d')

    x = np.arange(-3, 3, 0.05)
    y = np.arange(-3, 3, 0.05)

    X, Y = np.meshgrid(x, y)

    # https://stackoverflow.com/questions/8722735/i-want-to-use-matplotlib-to-make-a-3d-plot-given-a-z-function
    z = np.array([benchmark.rastrigin([x, y]) for x, y in zip(np.ravel(X), np.ravel(Y))])
    Z = z.reshape(X.shape)

    ax.plot_surface(X, Y, Z, cmap=plt.get_cmap('viridis'))

    if file_path is not None:
        print 'Saving to file %s' % file_path
        plt.savefig(file_path, dpi=300)
    else:
        plt.show()

    plt.close()


def test_schwefel(file_path=None):
    """
    :param file_path: file to save function to
    :type file_path: str
    """

    fig = plt.figure()
    ax = plt.gca(projection='3d')

    x = np.arange(-500, 500, 0.5)
    y = np.arange(-500, 500, 0.5)

    X, Y = np.meshgrid(x, y)

    # https://stackoverflow.com/questions/8722735/i-want-to-use-matplotlib-to-make-a-3d-plot-given-a-z-function
    z = np.array([benchmark.schwefel([x, y]) for x, y in zip(np.ravel(X), np.ravel(Y))])
    Z = z.reshape(X.shape)

    ax.plot_surface(X, Y, Z, cmap=plt.get_cmap('viridis'))

    if file_path is not None:
        print 'Saving to file %s' % file_path
        plt.savefig(file_path, dpi=300)
    else:
        plt.show()

    plt.close()


def test_schaffer(file_path=None):
    """
    :param file_path: file to save function to
    :type file_path: str
    """

    fig = plt.figure()
    ax = plt.gca(projection='3d')

    x = np.arange(-10, 10, 0.1)
    y = np.arange(-10, 10, 0.1)

    X, Y = np.meshgrid(x, y)

    # https://stackoverflow.com/questions/8722735/i-want-to-use-matplotlib-to-make-a-3d-plot-given-a-z-function
    z = np.array([benchmark.schaffer([x, y]) for x, y in zip(np.ravel(X), np.ravel(Y))])
    Z = z.reshape(X.shape)

    ax.plot_surface(X, Y, Z, cmap=plt.get_cmap('plasma'))

    if file_path is not None:
        print 'Saving to file %s' % file_path
        plt.savefig(file_path, dpi=300)
    else:
        plt.show()

    plt.close()
