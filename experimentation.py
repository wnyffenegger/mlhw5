from approaches.artificial import Artificial
from approaches.dichotomy import Dichotomy
from material import SearchReport, GeneticAlgorithm
from approaches.dgea import DGEA
from approaches.island import Island

import math
import matplotlib.pyplot as plt
import os

DEFAULT_POP_SIZE = 50
DEFAULT_MAX_ITERATIONS = 1000
DEFAULT_INTERVAL = 20

DEFAULT_REPL_RATE = 0.25
DEFAULT_TOURN_FAVOR = 0.85
DEFAULT_MUT_RATE = 0.05
DEFAULT_ELITISM = 1

DEFAULT_STOP_CRITERION = 0.1

DEFAULT_BLX_ALPHA = 0.5


class Experiment:

    def __init__(self, name, benchmark, approach, pop_size, max_generations, num_genes, threshold):
        """
        :type name: str
        :type benchmark: str
        :type approach: str
        :type pop_size: int
        :type max_generations: int
        :type num_genes: int
        :type threshold: float
        """

        self.name = name
        self.benchmark = benchmark
        self.approach = approach
        self.pop_size = pop_size
        self.max_generations = max_generations
        self.num_genes = num_genes
        self.threshold = threshold

        self.repl_rate = DEFAULT_REPL_RATE
        self.tourn_favor = DEFAULT_TOURN_FAVOR
        self.mut_rate = DEFAULT_MUT_RATE
        self.blx_alpha = DEFAULT_BLX_ALPHA
        self.elitism = DEFAULT_ELITISM
        self.recording_interval = DEFAULT_INTERVAL

        self.special = dict()

    def __str__(self):

        printstr = ''
        printstr += 'Name: %s, Approach %s, Benchmark %s\n' % (self.name, self.approach, self.benchmark)
        printstr += 'Population Size: %d\n' % self.pop_size
        printstr += 'Max Generations: %d\n' % self.max_generations
        printstr += 'Dimensions: %d\n' % self.num_genes
        printstr += 'Replacement Rate: %0.2f\n' % self.repl_rate
        printstr += 'Mutation Rate: %0.2f\n' % self.mut_rate
        printstr += 'BLX-Alpha: %0.2f\n' % self.blx_alpha
        printstr += 'Elitism: %d\n' % self.elitism

        for key in self.special.keys():
            printstr += '%s: %0.4f\n' % (key, self.special[key])

        return printstr


def dgea_approach(exp, fitness):
    """
    Use DGEA to solve the function minimization problem

    :param exp:  script / description of experiment
    :type exp: Experiment
    :param fitness: fitness function to conduct experiment with
    :type fitness: Fitness
    :return: summary of experiment
    :rtype: SearchReport
    """

    expl_threshold = exp.special['expl_threshold']

    approach = DGEA(fitness, expl_threshold, exp.repl_rate, exp.mut_rate, exp.tourn_favor, exp.blx_alpha,
                    exp.elitism, exp.recording_interval)
    ga = GeneticAlgorithm(approach, exp.pop_size, exp.max_generations)

    return ga.run()


def island_approach(exp, fitness):
    """
    Use isolated islands of individuals to solve the function minimization problem

    :param exp:  script / description of experiment
    :type exp: Experiment
    :param fitness: fitness function to conduct experiment with
    :type fitness: Fitness
    :return: summary of experiment
    :rtype: SearchReport
    """

    # Number of islands to search
    islands = exp.special['islands']

    # Max generations between cross pollination
    pollination_interval = exp.special['pollination_interval']

    # Percent of individuals in island to be replaced by individuals
    # from another island
    pollination_ratio = exp.special['pollination_ratio']

    # Diversity below which cross pollination becomes required
    diversity_threshold = exp.special['diversity_threshold']

    approach = Island(fitness, islands, pollination_interval, pollination_ratio, diversity_threshold, exp.repl_rate,
                      exp.mut_rate, exp.tourn_favor, exp.blx_alpha, exp.elitism, exp.recording_interval)

    ga = GeneticAlgorithm(approach, exp.pop_size, exp.max_generations)

    report = ga.run()
    report.special['Cross Pollinations'] = approach.cross_pollinations

    return report


def artificial_approach(exp, fitness):
    """
    Use artificially inserted individuals to solve the function minimization problem

    :param exp:  script / description of experiment
    :type exp: Experiment
    :param fitness: fitness function to conduct experiment with
    :type fitness: Fitness
    :return: summary of experiment
    :rtype: SearchReport
    """

    # Minimum distance between artificially created individuals and every individual in the population
    artificial_threshold = exp.special['artificial_threshold']

    # Percent of individuals to be replaced with artificial individuals if ratio falls too low
    artificial_ratio = exp.special['artificial_ratio']

    # Minimum diversity to avoid artifical individual generation
    diversity_threshold = exp.special['diversity_threshold']

    approach = Artificial(fitness, diversity_threshold, artificial_threshold, artificial_ratio, exp.repl_rate,
                          exp.mut_rate, exp.tourn_favor, exp.blx_alpha, exp.elitism, exp.recording_interval)

    ga = GeneticAlgorithm(approach, exp.pop_size, exp.max_generations)

    report = ga.run()
    report.special['Artificially Inserted Individuals'] = approach.artificial_individuals

    return report


def dichotomy_approach(exp, fitness):
    """
    Use artificially inserted individuals to solve the function minimization problem

    :param exp:  script / description of experiment
    :type exp: Experiment
    :param fitness: fitness function to conduct experiment with
    :type fitness: Fitness
    :return: summary of experiment
    :rtype: SearchReport
    """

    # Minimum distance between artificially created individuals and every individual in the population
    focus = exp.special['focus']

    # Percent of individuals to be replaced with artificial individuals if ratio falls too low
    hof_size = exp.special['hof_size']

    approach = Dichotomy(fitness, focus, hof_size, exp.repl_rate, exp.mut_rate, exp.tourn_favor,
                         exp.blx_alpha, exp.elitism, exp.recording_interval)

    ga = GeneticAlgorithm(approach, exp.pop_size, exp.max_generations)

    report = ga.run()
    report.special['Dissimilarity Calculations'] = approach.dissimilar_calcs

    return report


def summarize(exp, reports, folder_path):
    """
    Summarize experimental results
    :param exp: description of experiment
    :type exp: Experiment
    :param reports: list of reports for experiments
    :type reports: list(SearchReport)
    :param folder_path: path to folder
    :type folder_path: str
    """

    folder_path = folder_path + "/" + exp.benchmark
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

    # Average final statistics
    final_statistics = dict()

    for key in reports[0].final.keys():
        final_statistics[key] = 0.0

    for report in reports:
        for key, value in report.final.iteritems():
            final_statistics[key] += value

    for key, value in final_statistics.iteritems():
        final_statistics[key] = value / len(reports)

    # Save experiment to file with average final statistics
    with file(folder_path + '/summary.txt', 'w') as summary:

        summary.write(str(exp))
        summary.write('\n')
        summary.write('Average Final Statistics\n')

        for key, value in final_statistics.iteritems():
            summary.write('%s : %0.2f\n' % (key, value))


def evaluate_experiment(exp, reports, folder_path, save_graphs=False):
    """
    Generate standard set of graphs for each tracked value

    There is a better way to do this using list comprehensions and python's builtin functions but this will do for now

    :param exp: description of experiment
    :type exp: Experiment
    :param reports: list of reports for experiments
    :type reports: list(SearchReport)
    :param folder_path: path to folder
    :type folder_path: str
    :param save_graphs: save graphs to file
    :type save_graphs: bool
    :return: a dictionary of data from the experiments
    :rtype: dict
    """

    results = dict()
    hits = dict()

    for key in reports[0].record.keys():
        results[key] = dict()
        hits[key] = dict()

    # Collect all data
    for report in reports:
        for key, data in report.record.iteritems():
            # Each data point is stored as a tuple of the generation the point was taken from and the value
            for gen, datapoint in data:
                if gen not in hits[key]:
                    hits[key][gen] = 1
                else:
                    hits[key][gen] += 1

                if gen not in results[key]:
                    results[key][gen] = float(datapoint)
                else:
                    results[key][gen] += datapoint

    # Average all data try to account for some searches taking longer than others
    for key in results.keys():
        for gen, datapoint in results[key].iteritems():
            # Only divide by number of points available for that generation
            results[key][gen] /= hits[key][gen]

    organized_data = dict()
    for key in results.keys():
        x_axis = results[key].keys()
        x_axis.sort()

        y_axis = [results[key][gen] for gen in x_axis]
        organized_data[key] = x_axis, y_axis, key, exp.approach

    if save_graphs:
        folder_path = folder_path + "/" + exp.benchmark
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)

        for key in organized_data.keys():
            build_graph("%s - %s (%s)" % (exp.name, exp.benchmark, key), "Generations", key,
                        organized_data[key], '%s/%s.png' % (folder_path, key))

    return organized_data


def build_graph(title, x_title, y_title, data, file_name):
    """
    :param title: graph title
    :type title: str
    :param x_title: x axis title
    :type x_title: str
    :param y_title: y axis title
    :type y_title: str
    :param data: set of data to display
    :type data: tuple(list(int), list(float), str)
    :param file_name: file to save graph to
    :type file_name: str
    :return:
    """

    fig, ax = plt.subplots()
    ax.plot(data[0], data[1])

    ax.set(xlabel=x_title, ylabel=y_title, title=title)

    plt.savefig(file_name, dpi=300)
    plt.close()


def build_multi_graph(title, x_title, y_title, data, file_name):
    """
    :param title: graph title
    :type title: str
    :param x_title: x axis title
    :type x_title: str
    :param y_title: y axis title
    :type y_title: str
    :param data: set of data to display
    :type data: list(tuple(list(int), list(float), str))
    :param file_name: file to save graph to
    :type file_name: str
    :return:
    """

    fig, ax = plt.subplots()

    for d in data:
        ax.plot(d[0], [math.log(el) for el in d[1]], label=d[3])

    ax.set(xlabel=x_title, ylabel='log(%s)' % y_title, title=title)
    legend = ax.legend(loc='best', title="Approach", fontsize='x-large')
    plt.savefig(file_name, dpi=300)
    plt.close()
